function cambiaVista(elem){
	if(elem.attr('view')=='index.html'){
		window.location.href='index.html';
	} else {
		var url='views/'+elem.attr('view');
	}
    $.ajax({
        type: 'get',
        url: url,
        data: null,
        success: function(data) {
            $('#content').html(data)
            var partes=url.split('/');
            setTimeout(function(){ 
              inicializaciones(partes[1],partes[2]);
              $('.contList').fadeIn(500);
            },150);
        },
        error: function(jqXHR, textStatus, errorThrown) {
            alert("Error detectado: " + textStatus + "\nExcepcion: " + errorThrown);
        }
    });
}

function inicializaciones(juego,vista){
    if(juego=='diceForge'){
        inicioDiceForge(vista);
    } 
}

function inicioDiceForge(vista){
    if(vista=='index.html'){
        var expansiones = localStorage.getItem('diceForge_expansiones') || 'Ninguna';
        $('.diceForge #expansiones[value='+expansiones+']').prop('checked',true);
        $('.diceForge #expansiones').change(function(){
            localStorage.setItem("diceForge_expansiones",$(this).val());
        });

        var promo='';
        var tienePromo='';
        $(".diceForge .checkPromos").each(function( index ) {
            promo=$(this).val();
            tienePromo = localStorage.getItem('diceForge_'+promo) || 'NO';
            if(tienePromo=='SI'){
                $(this).prop('checked',true);
            }
            $(this).change(function(){
                promo=$(this).val();
                if($(this).attr('checked')=='checked'){
                    localStorage.setItem("diceForge_"+promo,'SI');
                } else {
                    localStorage.setItem("diceForge_"+promo,'NO');
                }
            });
        });

        var color='';
        var tieneColor='';
        var jugadores=0;
        var jugadoresMarcados=0;
        $(".diceForge .checkColores").each(function( index ) {
            color=$(this).val();
            tieneColor = localStorage.getItem('diceForge_'+color) || 'NO';
            if(tieneColor=='SI'){
                $(this).prop('checked',true);
            }
            $(this).change(function(e){
                color=$(this).val();
                if($(this).attr('checked')=='checked'){
                    jugadores=$('select#jugadores option:selected').val();
                    jugadoresMarcados=cuantosJugadoresDiceForge();
                    if(jugadoresMarcados>jugadores){
                        alert('Para marcar otro color, debes añadir más jugadores');
                        $(this).prop('checked',false);
                    } else {
                        localStorage.setItem("diceForge_"+color,'SI');
                    }
                } else {
                    localStorage.setItem("diceForge_"+color,'NO');
                }
            });
        });
        var iColor=0;
        var colores=['Azul','Verde','Negro','Naranja'];
        var jugadores = localStorage.getItem('diceForge_jugadores') || '2';
        $('select#jugadores').val(jugadores).selectpicker('refresh');
        $('select#jugadores').change(function(){
            jugadoresMarcados=cuantosJugadoresDiceForge();
            jugadores=$(this).val();
            localStorage.setItem("diceForge_jugadores",jugadores);
            while(jugadores!=jugadoresMarcados){
                hecho=false;
                while(!hecho && iColor<4){
                    if(jugadores>jugadoresMarcados){
                        if($('.checkColores[value='+colores[iColor]+']').attr('checked')!='checked'){
                            $('.checkColores[value='+colores[iColor]+']').prop('checked',true);
                            hecho=true;
                            localStorage.setItem("diceForge_"+colores[iColor],'SI');
                        }
                    } else {
                        if($('.checkColores[value='+colores[iColor]+']').attr('checked')=='checked'){
                            $('.checkColores[value='+colores[iColor]+']').prop('checked',false);
                            hecho=true;
                            localStorage.setItem("diceForge_"+colores[iColor],'NO');
                        }
                    }
                    iColor++;
                }
                iColor=0;
                jugadoresMarcados=cuantosJugadoresDiceForge();
            }
        }); 
    } else if(vista=='tablero.html'){
        eligeCartasDiceForge();
    }
}

function cuantosJugadoresDiceForge(){
    res=0;
    $(".diceForge .checkColores").each(function( index ) {
        if($(this).attr('checked')=='checked'){
            res++;
        }
    });
    return res;
}

function eligeCartasDiceForge(){
    var pos=['s1','s2','s3','s4','s5','s6','s7','l1','l2','l3','l4','l5','l6','l7','h'];
    var cartas=[
        ['El anciano'],
        ['Espiritus salvajes'],
        ['Buho del guardián','Barco celestial'],
        ['Minotauro','Escudo del guardián'],
        ['Gorgona','Triton'],
        ['Espejo del abismo'],
        ['Esfinge','Cíclope'],
        ['Martillo del herrero'],
        ['Cofre del herrero'],
        ['La cierva plateada','Oso gigante'],
        ['Sátiro','Jabalí obstinado'],
        ['Barquero','Cerbero'],
        ['Yelmo de invisibilidad'],
        ['Cancer','Centinela'],
        ['Hidra','Tifon'] 
    ];
    cartas=comprobarPromos(cartas);
    var expansion = localStorage.getItem('diceForge_expansiones') || 'Ninguna';
    if(expansion!='Ninguna'){
        var expansionCartas=[
            ['El árbol','El mercader'],
            ['La ninfa del bosque'],
            [],
            ['La Luz'],
            ['El orfebre','El omnisapiente'],
            ['Tridente abisal','Espejo de la desventura'],
            ['El fuego eterno','La mano izquierda'],
            ['Las gemelas','Cetro del herrero'],
            ['El compañero'],
            [],
            ['El viento','El dado celestial'],
            ['Las brumas','EL ancestro'],
            [],
            ['La mano derecha','La noche eterna'],
            ['El primer Titán','La Diosa'] 
        ];
        var nuevas;
        for(var i=0;i<cartas.length;i++){
            nuevas=expansionCartas[i];
            for(var j=0;j<nuevas.length;j++){
                cartas[i].push(nuevas[j]);
            }
        }
        if(expansion=='Diosa'){
            cartas[2]=['El gólem solar'];
            cartas[3]=['El gólem del tiempo'];
            cartas[9]=['El gólem lunar'];
            cartas[12]=['El gran golem'];
        } else if(expansion=='Titan'){
            cartas[1]=['El obstinado'];
            cartas[2]=['El guardián'];
            cartas[8]=['El recuerdo'];
            cartas[9]=['El oráculo'];
            cartas[12]=['El caos'];
        }
    }
    var jugadores = localStorage.getItem('diceForge_jugadores') || '2';
    var promosh=['Quimera','Madre de monstruos','Diosa sombra'];
    var promosh2=new Array()
    var res='';
    var s='';
    var hibridas=0;
    var resto=0
    for(var i=0;i<cartas.length;i++){
        s=Math.floor(Math.random() * cartas[i].length);
        if(promosh.includes(cartas[i][s])){
            res='<div class="nombreCarta">'+cartas[i][s]+'</div><div class="cantidadCarta">x1</div>';
            $('#'+pos[i]).html(res);
            promosh2.push(cartas[i][s]);
            hibridas++;
            while(hibridas<jugadores){
                s=Math.floor(Math.random() * cartas[i].length);
                if(promosh.includes(cartas[i][s])){
                    if(promosh2.includes(cartas[i][s])==false){
                        res='<div class="nombreCarta">'+cartas[i][s]+'</div><div class="cantidadCarta">x1</div>';
                        hibridas++;
                        cambiarClasesHibridas(hibridas);
                        $('#h'+(hibridas)).html(res);
                        $('#h'+(hibridas)).css('display','block');
                        promosh2.push(cartas[i][s]);
                    }
                } else {
                    resto=jugadores-hibridas;
                    hibridas++;
                    cambiarClasesHibridas(hibridas);
                    res='<div class="nombreCarta">'+cartas[i][s]+'</div><div class="cantidadCarta">x'+(resto)+'</div>';
                    $('#h'+(hibridas)).html(res);
                    $('#h'+(hibridas)).css('display','block');
                    hibridas=jugadores;
                }
            }
        } else {
            cambiarClasesHibridas(hibridas);
            res='<div class="nombreCarta">'+cartas[i][s]+'</div><div class="cantidadCarta">x'+jugadores+'</div>';
            $('#'+pos[i]).html(res);
        }
    }
    var colores=['Azul','Verde','Negro','Naranja'];
    var color='';
    var orden=new Array();
    for(i=0;i<colores.length;i++){
        tieneColor = localStorage.getItem('diceForge_'+colores[i]) || 'NO';
        if(tieneColor=='SI'){
            orden.push('<div class="'+colores[i]+'"></div>');
        }
    }
    orden.sort(function() {return Math.random() - 0.5});
    for(i=0;i<orden.length;i++){
        $('#orden').append(orden[i]);
    }
    $('#orden').append('<div id="texto">ORDEN: </div>');
}

function cambiarClasesHibridas(i){
    if(i==2){
        $('#h').addClass('con2');
    } else if(i==3){
        $('#h').addClass('con3');
        $('#h2').addClass('con3');
    } else if(i==4){
        $('#h').addClass('con4');
        $('#h2').addClass('con4');
        $('#h3').addClass('con4');
    }
}

function comprobarPromos(cartas){
    var tienePromo = localStorage.getItem('diceForge_Ninfa') || 'NO';
    if(tienePromo){
        cartas[0].push('Ninfa');
    }
    tienePromo = localStorage.getItem('diceForge_Quimera') || 'NO';
    if(tienePromo){
        cartas[14].push('Quimera');
    }
    tienePromo = localStorage.getItem('diceForge_Madre') || 'NO';
    if(tienePromo){
        cartas[14].push('Madre de monstruos');
    }
    tienePromo = localStorage.getItem('diceForge_Pegaso') || 'NO';
    if(tienePromo){
        cartas[3].push('Pegaso');
    }
    tienePromo = localStorage.getItem('diceForge_Ruleta') || 'NO';
    if(tienePromo){
        cartas[4].push('Ruleta de la fortuna');
    }
    tienePromo = localStorage.getItem('diceForge_Diosasombra') || 'NO';
    if(tienePromo){
        cartas[14].push('Diosa sombra');
    }
    return cartas
}

function campoRadio(dentro,texto,name,valorPorDefecto='NO',textosCampos=['Si','No'],valoresCampos=['SI','NO'],salto='NO'){
  var res="<div class='control-group'>";
  res+="<label class='control-label' for='"+name+"'>"+texto+":</label>";
  res+="<div class='controls nowrap'>";
  for(var i=0;i<textosCampos.length;i++){
    res+="<label class='radio inline'>";
    res+="<input type='radio' name='"+name+"' id='"+name+"' value='"+valoresCampos[i]+"'";
    if(valoresCampos[i]==valorPorDefecto){
      res+=" checked='checked'";
    }
    res+=">"+textosCampos[i]+"</label>";
    if(salto=='SI'){
      res+="<br/>";
    }
  }
  res+="</div></div>";
  if(dentro==''){
    $('form').append(res);
  } else {
    dentro.append(res);
  }
}

function campoCheckIndividual(dentro,texto,name,valorCampo='SI',clase=''){
  var res="<label class='checkbox inline'>";
  res+="<input type='checkbox' class='"+clase+"' name='"+name+"' id='"+name+"' value='"+valorCampo+"'>";
  res+=texto+"</label><br/>";
  if(dentro==''){
    $('form').append(res);
  } else {
    dentro.append(res);
  }
}

function campoSelect(dentro,texto,name,nombres,valores,clase='selectpicker span3 show-tick',busqueda="data-live-search='true'",conNull='SI'){
  var res="<div class='control-group'>";
  res+="<label class='control-label' for='"+name+"'>"+texto+":</label>";
  res+="<div class='controls'>";
  res+="<select name='"+name+"' class='"+clase+"' id='"+name+"' "+busqueda+">";
  if(conNull=='SI'){
    res+="<option value='NULL'></option>";
  }
  for(var i=0;i<nombres.length;i++){
    if(valores[i]!=''){
      res+="<option value='"+valores[i]+"'>"+nombres[i]+"</option>";
    }
  }
  res+="</select></div></div>";
  if(dentro==''){
    $('form').append(res);
  } else {
    dentro.append(res);
  }
  iniciaCamposSelect(name);
}

function iniciaCamposSelect(id){
  $('#'+id).selectpicker();
}

function botonNavegacion(dentro,texto,icono,view,clase=''){
    var res='<button class="'+clase+' itemNavigation" view="'+view+'"><i class="'+icono+'"></i> '+texto+'</button>';
    if(dentro==''){
        $('form').append(res);
    } else {
        dentro.append(res);
    }
}